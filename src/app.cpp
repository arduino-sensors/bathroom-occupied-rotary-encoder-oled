#include <Arduino.h>
#include <avr/sleep.h>
#include <Encoder.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

// Change these pin numbers to the pins connected to your encoder.
//   Best Performance: both pins have interrupt capability
//   Good Performance: only the first pin has interrupt capability
//   Low Performance:  neither pin has interrupt capability
Encoder rotaryEncoder(2, 3);
const int PinSW=4;    // Reading Push Button switch
long encoderPosition  = -999;

// RGB Pins
#define RED 11
#define GREEN 9
#define BLUE 10

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

//Test that OLED height is correct in header file
#if (SSD1306_LCDHEIGHT != 64)
  #error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

// define variables
int redValue = 255;
int greenValue = 0;
int blueValue = 0;

const unsigned long secDivisor=1000;
const unsigned long minDivisor=secDivisor*60;
const unsigned long hoursDivisor=minDivisor*60;
const unsigned long daysDivisor=hoursDivisor*24;

//Used to track if we should power down
long arduinoClockTimeAtStateChange = millis();

const String DAYS_UNIT = "days";
const String HOURS_UNIT = "hrs";
const String MINUTES_UNIT = "min";
const String SECONDS_UNIT = "sec";
const String MILLIS_UNIT = "millis";
const String messageQuadrantOne = "Empty";
const String messageQuadrantTwo = "Occupied";
const String messageQuadrantThree = "Airing";
const String messageQuadrantFour = "Showering";

const int LOOP_DELAY = 100;

void setup() {
  Serial.begin(115200);
  Serial.println("Setting Up");

  // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)

  display.print("Initializing");
  delay(2000);
  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(BLUE, OUTPUT);
  digitalWrite(RED, LOW);
  digitalWrite(GREEN, LOW);
  digitalWrite(BLUE, LOW);

  pinMode(PinSW,INPUT);

  digitalWrite(PinSW, HIGH); // Pull-Up resistor for switch
  // Clear the buffer.
  display.clearDisplay();
  // init done
  Serial.println("Set Up Done");
}

//Prints font size 2 in first 16 pixes at 0,0
void printMessage(String message, int textSize, boolean white) {
  display.setTextSize(textSize);
  if (white){
    display.setTextColor(WHITE);
  } else {
    display.setTextColor(BLACK);
  }
  display.setCursor(0,0);
  display.println(message);
  display.display();
}

void printMessage(String message, int y, int x, int textSize, boolean white) {
  display.setTextSize(textSize);
  if (white){
    display.setTextColor(WHITE);
  } else {
    display.setTextColor(BLACK);
  }
  display.setCursor(x,y);
  display.println(message);
  display.display();
}

String getUnits(long duration){
  if (duration/daysDivisor>0) {
    return DAYS_UNIT;
  } else if (duration/hoursDivisor>0){
    return HOURS_UNIT;
  } else if (duration/minDivisor>0){
    return MINUTES_UNIT;
  } else if (duration/secDivisor>0){
    return SECONDS_UNIT;
  }
  return MILLIS_UNIT;
}

long getRoundedUnit(long duration){
  if (duration/daysDivisor>0) {
    return duration/daysDivisor;//Days
  } else if (duration/hoursDivisor>0){
    return duration/hoursDivisor;//Hours
  } else if (duration/minDivisor>0){
    return duration/minDivisor;//minutes
  } else if (duration/secDivisor>0){
    return duration/secDivisor;//seconds
  }
  return duration;
}

// here we put the arduino to sleep
void sleepNow() {
  long timeSinceLastStateChange = millis() - arduinoClockTimeAtStateChange;

  String units = getUnits(timeSinceLastStateChange);
  long roundedUnit = getRoundedUnit(timeSinceLastStateChange);

  if (units == HOURS_UNIT && roundedUnit >= 1){
    // Clear the buffer.
    display.clearDisplay();
    display.dim(true);
    digitalWrite(RED, LOW);
    digitalWrite(GREEN, LOW);
    digitalWrite(BLUE, LOW);

    set_sleep_mode(SLEEP_MODE_PWR_DOWN);   // sleep mode is set here
    sleep_enable(); // enables the sleep bit in the mcucr register so sleep is possible. just a safety pin
  	sleep_mode();  // here the device is actually put to sleep!! THE PROGRAM CONTINUES FROM HERE AFTER WAKING UP
  }
}

void changeLED(){
  analogWrite(RED, redValue);
  analogWrite(GREEN, greenValue);
  analogWrite(BLUE, blueValue);
}

String handleState(int quadrant){
  if (quadrant == 1) {
    redValue = 0;
    greenValue = 204;
    blueValue = 0;
    changeLED();
    return messageQuadrantOne;
  } else if (quadrant == 2) {
    redValue = 204;
    greenValue = 0;
    blueValue = 0;
    changeLED();
    return messageQuadrantTwo;
  } else if (quadrant == 3) {
    redValue = 0;
    greenValue = 0;
    blueValue = 204;
    changeLED();
    return messageQuadrantThree;
  } else if (quadrant == 4) {
    redValue = 255;
    greenValue = 51;
    blueValue = 0;
    changeLED();
    return messageQuadrantFour;
  }
  return messageQuadrantOne;
}

void updateMessage(String statusMessage){
  delay(20);
  display.clearDisplay();
  printMessage(statusMessage, 40, 0, 2, true);
}

int determineQuadrant(long encoderReading){
  int quadrant = 0;
  if (encoderReading <0){
    if (encoderReading > -21) {
      quadrant = 4;
    } else if (encoderReading<-20 && encoderReading > -41) {
      quadrant = 3;
    } else if (encoderReading<-40 && encoderReading > -61) {
      quadrant = 2;
    } else if (encoderReading<-60 && encoderReading > -80) {
      quadrant = 1;
    }
  } else {
    if (encoderReading>=0 && encoderReading < 21) {
      quadrant = 1;
    } else if (encoderReading>20 && encoderReading < 41) {
      quadrant = 2;
    } else if (encoderReading>40 && encoderReading < 61) {
      quadrant = 3;
    } else if (encoderReading>60 && encoderReading < 80) {
      quadrant = 4;
    }
  }
  return quadrant;

}

void handleRotaryClick(int quadrant){
  arduinoClockTimeAtStateChange=millis();
  Serial.print("Clicked. encoderPosition=");
  Serial.print(encoderPosition);
  Serial.print("  quadrant=");
  Serial.println(quadrant);
  digitalWrite(PinSW, HIGH); // Pull-Up resistor for switch
  int newEncoderPosition = encoderPosition;
  if (encoderPosition>=60 || encoderPosition <= -60){
    rotaryEncoder.write(0);
  } else {
    if (encoderPosition == 0){
      newEncoderPosition = 24;
    } else if (encoderPosition  > 0){
      newEncoderPosition = newEncoderPosition + 20;
    } else {
      newEncoderPosition = newEncoderPosition - 20;
    }
    rotaryEncoder.write(newEncoderPosition);
  }
}

void loop() {
  delay(LOOP_DELAY);
  boolean updateDisplay = false;
  long newEncoderPosition;
  newEncoderPosition = rotaryEncoder.read();
  if (newEncoderPosition != encoderPosition) {
    encoderPosition = newEncoderPosition;
    if (encoderPosition>=80||encoderPosition<=-80){
      rotaryEncoder.write(0);
    }
    if (encoderPosition%4 == 0){
      updateDisplay = true;
    }
  }
  sleepNow();
  int quadrant = determineQuadrant(encoderPosition);
  if (!(digitalRead(PinSW))) {   // check if button is pressed
    delay(LOOP_DELAY);
    handleRotaryClick(quadrant);
  }

  if (updateDisplay){
    arduinoClockTimeAtStateChange=millis();
    Serial.print("encoderPosition=");
    Serial.print(encoderPosition);
    Serial.print("  |  quadrant=");
    Serial.println(quadrant);
    String statusMessage = handleState(quadrant);
    updateMessage(statusMessage);
  }
}
