# Arduino Nano & Rotary Encoder with OLED Display for updating Bathroom Occupied/Unoccupied status

### Sets whether or not the bathroom is occupied or not
## Overview
OLED is 64x128.

Uses 4 quadrants of Rotary encoder to set 4 states of bathroom:
`const String messageQuadrantOne = "Empty";`
`const String messageQuadrantTwo = "Showering";`
`const String messageQuadrantThree = "Damage    Control";`
`const String messageQuadrantFour = "Occupied";`

Clicking the encoder button resets state to 0 (Empty). Quandrants are defined based on position at time of reset.

Using extenal libraries:
* Arduino.H
* Encoder.H
* SPI.h
* Wire.h
* Adafruit_GFX.h
* Adafruit_SSD1306.h

## Mapping of Board and component pins to custom built board pins mapping

* Arduino GND Pin - pin 1 on external - Ground
* Arduino 3.5V Pin - pin 2 on external - 5V power
* RGB RED left of ground - digital pin 11 - pin 3 on external
* RGB GREEN right of ground - digital pin 9 - pin 4 on external
* RGB BLUE far right of ground - digital pin 10 - pin 5 on external
* OLED SCL - analog pin 4 - pin 6 on external
* OLED SDA - analog pin 5 - pin 7 on external
* Encoder SW - digital pin 4 - pin 8 on external
* Encoder DT - digital pin 3 - pin 9 on external
* Encoder CLK - digital pin 2 - pin 10 on external
